<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sellrentmiami');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Nu[XLyu!luS8s*VO&Rel2b3D^ !nEQ{u;+M@^qr2D&q]8&X=Yf |C[?g_>~VNTs1');
define('SECURE_AUTH_KEY',  'nJAI;M+7*n;(q@$f~{gn[CRCB|w_KPw8,nI>zdG2Hrd8hG>|cc}lREm1nT`u?e9*');
define('LOGGED_IN_KEY',    '+2xo01qu$Yy9yiMW<N+c;1tUxQ9j A9l:]n1P%5U=@{Wv)-g+%T5P=JQcT|511&f');
define('NONCE_KEY',        '.OI1%NtH$a$7__}Kj!&2:?aK)?-F/-Q2$D6[+KK%-PPqXs-l5#_;|iaU(]]F+h-|');
define('AUTH_SALT',        ' ~|m[B>NkPMlMxLbBeA:4TM`r?-aV&g:o{=?t?x-XDC-)96@E7QAgq8WV=Ew[.>G');
define('SECURE_AUTH_SALT', 'w_E8!rRmy;YQ}7?X/P}CaBPn)pj5k:bua-r#Y(J%UQtJ|m8-.aGRS0S7E/_f},1)');
define('LOGGED_IN_SALT',   'r_od@!M3N:@41Ji1m{hj]Tu:11y:KQ@ueTa+hc|^Yp[|IKqNA81>Lpq:ii2X_0F_');
define('NONCE_SALT',       'A@5;ozuVg{~:m4+XA+Mm;V<L4~lkZ]Mpj7q+,%1+#:dx2dq+Y2<NPtpH+--h[i#O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gfrs_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');