<?php
error_reporting(E_ALL ^ E_WARNING);

include('../wp-config.php');

$upload_dir = wp_upload_dir();

$photo_DIR = $upload_dir['basedir'] . '/listings/';

/*
 * Save post
 * Metadata fields => RETS field
 * _property_price => 137
 * _property_bathrooms => 92
 * _property_bedrooms => 25
 * _property_area => 261
 * _property_latitude, _property_longitude => 250
 * _property_slider_image => Front image
 * _property_slides
 */
function saveListing($listing = array())
{
    if (empty($listing))
        return false;

    // Single listing to save in WP
    $post['post_author'] = 1;
    $post['post_content'] = $listing['214'];
    $post['post_title'] = $listing['247'] . ' ' . $listing['248'] . ' ' . $listing['922'];
    $post['post_status'] = 'publish';
    $post['post_name'] = strtolower(preg_replace('/[^a-zA-Z0-9]/', '-', $listing['name']));
    $post['post_type'] = 'property';

    // Check exist
    $id_inserted_post = checkMetaSysid($listing['sysid']);
    if ($id_inserted_post) {
        $post['ID'] = $id_inserted_post->post_id;
        $id_inserted_post = wp_update_post($post);
    } else {
        $id_inserted_post = wp_insert_post($post);
    }

    if (!$id_inserted_post)
        return false;

    // Metapost
    update_post_meta($id_inserted_post, '_property_price', $listing['137']);
    update_post_meta($id_inserted_post, '_property_bathrooms', $listing['92']);
    update_post_meta($id_inserted_post, '_property_bedrooms', $listing['25']);
    update_post_meta($id_inserted_post, '_property_area', $listing['261']);
    update_post_meta($id_inserted_post, '_property_rets', $listing['sysid']);
    $meta_fields = array('_property_price', '_property_bathrooms', '_property_bedrooms', '_property_area', '_property_rets', '_property_slides', '_property_slider_image', '_property_latitude', '_property_longitude');
    update_post_meta($id_inserted_post, '_property_meta_fields', $meta_fields);
    $coord = getCoordinates($post['post_title']);
    update_post_meta($id_inserted_post, '_property_latitude', $coord['lat']);
    update_post_meta($id_inserted_post, '_property_longitude', $coord['lng']);

    return array('postid' => $id_inserted_post, 'sysid' => $listing['sysid']);
}

function savePhotosListing($idpost, $sysid, $photos = array())
{
    global $photo_DIR, $upload_dir;

    $c = 0;
    $new_slides = array();
    foreach ($photos as $photo) {
        $listing = $photo['Content-ID'];
        $number = $photo['Object-ID'];

        if ($photo['Success'] == true) {
            file_put_contents($photo_DIR . "image-{$listing}-{$number}.jpg", $photo['Data']);

            $new_slides[$c]['imgurl'] = $upload_dir['baseurl'] . '/listings/' . "image-{$listing}-{$number}.jpg";
            if ($c == 0) {
                $slider_img = $upload_dir['baseurl'] . '/listings/' . "image-{$listing}-{$number}.jpg";
                $slider_local_image = $photo_DIR . "image-{$listing}-{$number}.jpg";
            }
            $c++;
        } else {
            echo "Error downloading photo: ({$listing}-{$number}): {$photo['ReplyCode']} = {$photo['ReplyText']} \n";
        }
    }

    update_post_meta($idpost, '_property_slider_image', $slider_img);
    update_post_meta($idpost, '_property_slides', $new_slides);

    // Thumbnail
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    $wp_filetype = wp_check_filetype(basename($slider_img), null);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => preg_replace('/\.[^.]+$/', '', $slider_img),
        'post_content' => '',
        'post_status' => 'inherit'
    );

    $attach_id = wp_insert_attachment($attachment, $slider_local_image, $idpost);
    $attach_data = wp_generate_attachment_metadata($attach_id, $slider_local_image);
    wp_update_attachment_metadata($attach_id, $attach_data);
    update_post_meta($idpost, '_thumbnail_id', $attach_id);
}

function checkPropertyExist($post_name)
{
    global $wpdb;
    $post_name = strtolower(preg_replace('/[^a-zA-Z0-9]/', '-', $post_name));

    return $wpdb->get_row("SELECT ID FROM gfrs_posts WHERE post_name = '" . $post_name . "'");
}

function checkMetaSysid($sysid)
{
    global $wpdb;

    return $wpdb->get_row("SELECT post_id FROM gfrs_postmeta WHERE meta_key = '_property_rets' AND meta_value = '" . $sysid . "'");
}

function getCoordinates($address)
{
    $address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern

    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";

    $response = file_get_contents($url);

    $json = json_decode($response, TRUE); //generate array object from the response from the web

    return array( 'lat' => $json['results'][0]['geometry']['location']['lat'], 'lng' => $json['results'][0]['geometry']['location']['lng']);
}

?>