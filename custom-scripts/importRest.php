<?php
/**
 * Integrate PHRETS to import listings from MLS server
 * L3yR0y
 * lele140686@gmail.com
 */

// RETS Config
$rets_login_url = 'http://sef.rets.interealty.com/Login.asmx/Login';
$rets_username = 'fWaRiaS';
$rets_password = 'dhwf8697';
$rets_user_agent = 'fWaRiaS';
$rets_user_agent_password = 'dhwf8697';

// Listing fields
/*
 * sysid => RETSID
 * StreetName => 247
 * StreetNumber => 248
 * City => 922
 * Bedrooms => 25
 * BathsFull => 92
 * Approx. Sqft Total Area => 261
 * ListPrice => 137
 * Type of Property => 1
 * MapCoordinate => 250
 * ListingID => 157
 * Remarks => 214
 */
$fields = '247,248,922,25,92,261,137,1,250,157,214,sysid';

// use http://retsmd.com to help determine the names of the classes you want to pull.
// these might be something like RE_1, RES, RESI, 1, etc.
$property_classes = array('RE1');

require_once('../wp-content/phrets/phrets.php');
require_once('include.php');

$rets = new phRETS;

$rets->AddHeader("RETS-Version", "RETS/1.7.2");
$rets->AddHeader('User-Agent', $rets_user_agent);

echo "+ Connecting to {$rets_login_url} as {$rets_username} \n";
$connect = $rets->Connect($rets_login_url, $rets_username, $rets_password, $rets_user_agent_password);

// Checkin connection status
if ($connect) {
    echo "  + Connected \n";
} else {
    echo "  + Not connected: \n";
    print_r($rets->Error());
    exit;
}

require_once('wpInteract.php');

$cities = getRESTSCities();

$count = 0;

// Iterating by County
foreach ($counties as $short => $long) {

    // Iterating by City (County)
    foreach (${$short} as $city) {
        $short_city = in_array_r($city, $cities);
        $short_city = $short_city['ShortValue'];

        if (!$short_city)
            continue;

        // Searching by: Resource, Class, Status=Available, County, City
        $query = "(246=A),(61=$short),(922=$short_city)";
        echo "   + Query: $query  \n";
        $search = $rets->SearchQuery("Property", "1", $query, array('Select' => $fields));

        echo "    + Total found: {$rets->NumRows()} \n";

        // Working with listings found
        if ($rets->NumRows() > 0) {
            while ($listing = $rets->FetchRow($search)) {
                // Save listing (post)
                $idpost = saveListing($listing);

                // Get photos
                $photos = $rets->GetObject("Property", "Photo", $listing['sysid']);
                savePhotosListing($idpost['postid'], $idpost['sysid'], $photos);

                if ($count == 10) {
                    die('aaassss');
                }

                $count++;
            }

            $rets->FreeResult($search);
        }
    }
}

echo "+ Disconnecting \n";
$rets->Disconnect();

// Functions
function getRESTSCities()
{
    global $rets;
    $values = $rets->GetLookupValues("Property", "1_49"); // Cities

    return $values;
}

// Recursive in_array() for multidimensionals arrays
function in_array_r($needle, $haystack, $strict = false)
{
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return $item;
        }
    }

    return false;
}

?>